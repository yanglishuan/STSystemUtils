//
//  AppDelegate.h
//  STSystemUtils
//
//  Created by yls on 13-12-10.
//  Copyright (c) 2013年 yls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
